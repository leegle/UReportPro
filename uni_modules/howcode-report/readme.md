# howcode-report 云端数据报表插件说明

采用全端兼容的uCharts图表等插件完成的云端数据报表中心，用户只需要专注数据业务，即可打造出属于自己的数据报表中心，组件页面精美，使用起来灵活便捷，不仅如此，下载完整示例可搭配admin权限系统实现权限分配查看，最重要的是：`无偿供大家学习，无偿！`

## 推出云端数据报表中心初衷

howcode-report模拟了生活中各行业的数据分析，让不懂数据业务的小白也能尽快的上手数据表设计、数据分析、操作等流程，即使熟悉这些操作，数据报表的页面开发也是一件令人头疼的事，如今插件市场关于数据报表的插件寥寥无几，而howcode-report就提供了很多数据分析的报表案例，还原了真实场景下的数据报表中心开发，让用户在短时间内也可打造属于自己的高颜值报表中心

- 内嵌高度封装的各种数据插件，调用简便
- 支持H5、PC端，并且图表、字体大小完全适配PC端
- 采用uniCloud云端数据开发，真实还原数据报表的场景
- 图表生成使用了uCharts，其文档丰富，2.0升级后更是丰富了图表的开发

## 快速体验

![在这里插入图片描述](https://img-blog.csdnimg.cn/2021040514382389.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjAwMDgxNg==,size_16,color_FFFFFF,t_70#pic_center)


## 初始化配置
- 新建uni-app普通项目

- 插件市场点击右侧绿色按钮【使用HBuilderX导入插件】，或者【使用HBuilderX导入示例项目】查看完整示例工程

- 如果通过其他方式获取本组件，可直接右键项目选择 `新建uni_modules目录`，并且将`howcode-report`以及`quan-data-charts`插件粘贴到该目录中。注意：quan-data-charts非必须，可用howcode-report里面ucharts的jdk图表，但没有quan-data-charts插件方便，其属于ucharts的作者开发，建议搭配使用，具体自行查看代码

- 将uniCloud云函数文件复制到根目录下。注意：直接复制粘贴过来时会少粘贴uniCloud/cloudfunctions下的common文件，请自行在cloudfunctions目录下右键选择`新建common目录`，并把common目录下的内容拷贝过来

- 到[uniCloud控制台](https://unicloud.dcloud.net.cn)下创建一个属于自己的服务空间。注意：该步骤需要实名认证，并且确保账号与HBuilder登陆的账号一致，否则项目关联不上云服务空间

- 回到项目中选中uniCloud云空间，右键选中关联云服务空间，选择关联之前创建的服务空间

- 展开uniCloud文件，选中database目录下的db_init.json文件，右键选择`初始化云数据库`

- 选中cloudfunctions文件，右键选择`上传所有云函数、公共模块及action`

## 基本用法
- 在展示的page页面中直接写上`<howcode-report />`

```
<template>
	<view class="content">
		<howcode-report />
	</view>
</template>
```

注意：请保证当前页是个空白页面，否则会影响布局

## 示例文件地址：

`强烈建议先看本页帮助，再看下面示例文件源码！`

```
/components/data-center/page-one.vue（news新闻数据中心，读取uniCloud云端数据）

/components/data-center/page-two.vue（mall电商数据中心，读取uniCloud云端数据）

/components/data-center/page-three.vue（user-healthy会员健康数据中心，目前是静态数据）

/components/data-center/page-four.vue（user-service会员服务数据中心，目前是静态数据）
```
## 由于其他图表都有对应的插件文件，所以仅对progress-bar、data-progress和text-block封装的参数做说明
 ### 1. 进度条 (progress-bar)
 
 - 组件参数说明

	| params    | 类型   | 说明    |
	| ------    | ------ | ----------------|
	| content| array| 数据源|
	| isPC| boolean| 是否PC端，用于兼容PC端     |
	| isRank| boolean| 是否排序    |
	
- 数据说明

	| params    | 类型   | 说明 |
	| ------    | ------ | ------------|
	| name| string | 数据名称      |
	| num| string | 数据值   |
	| width| string | 数据条宽度，不需要传数据，通过计算获得  |
	| background| string | 不需要传数据，通过自动分配获得   |
	
 ### 2. 数据展示条 (data-progress)

 - 组件参数说明

	| params    | 类型   | 说明    |
	| ------    | ------ | ----------------|
	| progressList| array| 数据源|
	| isPC| boolean| 是否PC端，用于兼容PC端     |
	| borderRadius|number| 展示条圆角参数，PC端无效  |
	| padMiddle|number| 展示条间距 |

 - 数据说明
	| params    | 类型   | 说明                                       |
	| ------    | ------ | -------------------------------------------|
	| dataType  | string | 不传表示不展示目标值和现在值，传则展示       |
	| expect    | string | 目标值                                     |
	| now       | string | 现在达成值                                 |
	| value     | string | 现在达成值/目标值                           |

 ### 2. 文本块 (text-block)
  - 组件参数说明

	| params    | 类型   | 说明    |
	| ------    | ------ | ----------------|
	| content| array| 数据源|
	| isPC| boolean| 是否PC端，用于兼容PC端     |
	
- 数据说明

    | params | 类型           | 说明                                        |
    | ------ | -------------- | -------------------- |
    | kind  | string         | 表示使用哪种类型的文本块                       |
    | background    | string         | 背景颜色             |
    | content  | array | 该文本框内文本内容 |
	
- kind目前取值：1,2,3,4,5(注意：content中：文本放text中，数字放value，colortext为文本颜色，colorvalue为数字颜色)
- 1：分两层，上层纯数字，下纯文字 的排版类型
```
{
	"kind":1,
	"background":["#3EB2F5","#9374F7"],
	"content":[
		{"text":"","value":"5860","colortext":"","colorvalue":"#fff","size":"44rpx"},
		{"text":"新增微好友","value":"","colortext":"#fff","colorvalue":"","size":"24rpx"}
	]
}
```
- 2：分两层，上层文字+数字，下层文字+数字 的排版类型
```
{
	"kind":2,
	"background":["#B678FD","#4A64F9"],
	"content":[
		{"text":"环比增长","value":"3.2%","colortext":"#fff","colorvalue":"#fff","size":"24rpx"},
		{"text":"同比增长","value":"1.1%","colortext":"#fff","colorvalue":"#fff","size":"24rpx"}
	]
}
```
- 3：分三层，上层纯数字，中层纯文字，下层分左右块，左下层文+数，右下层文+数 的排版类型
```
{
	"kind":3,
	"background":["#B678FD","#4A64F9"],
	"content":[
		{"text":"","value":"38%","colortext":"","colorvalue":"#fff","size":"44rpx"},
		{"text":"小程序购买活跃会员占比","value":"","colortext":"#fff","colorvalue":"","size":"24rpx"},
		{"text":"同比","value":"2.5%","colortext":"#fff","colorvalue":"#fff","size":"20rpx"},
		{"text":"环比","value":"3.2%","colortext":"#fff","colorvalue":"#fff","size":"20rpx"}
	]
}
```
- 4：分三层，上层纯文字，中层纯数字，下层文+数， 的排版类型
```
{
	"kind":4,
	"background":["#F0F0F0","#F0F0F0"],
	"content":[
		{"text":"已评价数","value":"","colortext":"#000","colorvalue":"","size":"24rpx"},
		{"text":"","value":"161","colortext":"","colorvalue":"#09A1FD","size":"44rpx"},
		{"text":"增长","value":"","colortext":"#000","colorvalue":"","size":"20rpx"},
		{"text":"up","value":"325","colortext":"#DF297D","colorvalue":"#f25287","size":"20rpx"}
	]
}
```
- 5：分5层，具体可看会员运营-活跃会员的板块
```
{
	"kind":5,
	"background":["#F77E89","#F7953B"],
	"content":[
		{"text":"会员销售占比","value":"","colortext":"#fff","colorvalue":"","size":"24rpx"},
		{"text":"","value":"80%","colortext":"","colorvalue":"#fff","size":"44rpx"},
		{"text":"环比","value":"81.5%","colortext":"#fff","colorvalue":"#fff","size":"20rpx"},
		{"text":"同比","value":"-81.3%","colortext":"#fff","colorvalue":"#fff","size":"20rpx"},
		{"text":"平均参考水平","value":"85%","colortext":"#fff","colorvalue":"#fff","size":"20rpx"}
	]
},
```
## uni_modules目录说明

```
├── components
│ └── howcode-report──────────# 组件主入口模块
│ └── canvas────────────────# 通过ucharts的jdk封装的图表
│ └── data-center──────────────# 数据中心的页面内容
│ └── data-progress──────────────# 数据展示条组件
│ └── data-table──────────────# 数据表格组件
│ └── progress-bar──────────────# 进度条组件
│ └── text-block──────────────# 数据文本框组件
├── js_skd
│ └── u-charts用中转
│ ── └──config-ucharts.js ──────# uCharts默认配置文件（非APP端内可作为实例公用中转）
│ ── └──u-charts.js──────# uCharts基础库
├── static
│ └── api──────────────────# api目录
│ ── └──mall──────────# 电商api
│ ── └──news──────────# 新闻api
│ └── css──────────────────# 全局css
│ └── js──────────────────# 全局配置/公共方法
│ └── json──────────────────# 部分静态数据
│ └── table──────────────────# 表格的js
```
## 后期计划
- 逐渐将howcode-report的内置jdk生成的图表换成quan-data-charts插件生成的图表
- 完成更多的数据中心报表页面
- 探讨quan-data-charts下datacom的高阶用法

## 作者联系：1051495009@qq.com
