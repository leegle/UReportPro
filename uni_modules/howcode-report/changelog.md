## 1.0.6（2021-04-29）
- 删除页面的无关json引入
- 删除静态资源中的无用json，减轻插件包大小
## 1.0.5（2021-04-16）
- 去掉内置appid
- 交流群：878946748
## 1.0.4（2021-04-15）
- 更新qiun-data-charts插件
- 调整部分云函数api
- 去除小部分无用插件
## 1.0.3（2021-04-09）
- 改造滚动图表案例
- 完善滚动图表的拖动事件
- 完善部分图表偶尔获取不到dom
## 1.0.2（2021-04-08）
# 此次更新解决所有图表图层过高的问题
- 用qiun-data-charts插件替换所有jdk生成图表
- 部分静态数据适配qiun-data-charts格式
- 偶尔加载时会不出图表或者显示图表加载出错，切换tabBar重新加载即可，后期联合插件作者解决此问题
## 1.0.1（2021-04-07）
- 使用async/await进行顺序api调用
- 优化布局
- 将panel-canvas生成的图表改造成qiun-data-charts的
- 更新云函数
## 1.0.0（2021-04-06）
# 全新howcode-report数据报表中心插件上线
